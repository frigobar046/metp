%!TEX TS-program = xelatex
%!TEX encoding = UTF-8 Unicode
%!TEX root = ../metp.tex

\section{Elaborazione}

\subsection{La consolle di missaggio nelle sue architetture e nelle tipologie di utilizzo}

La consolle di missaggio (mixer) è l’elemento centrale nel processo dei suoni, sia per quel che riguarda la registrazione, sia per la diffusione, ed anche in quei casi in cui l’apparecchio non sia fisicamente presente, ma esista in forma virtuale, come nelle workstation dedicate o negli editor inseriti come software su computer, la sua architettura logica rimane nella sostanza la medesima, ed è quindi di fondamentale importanza la sua comprensione per poter approfondire le possibilità che essa offre. Il concetto base è quello di un dispositivo adatto a gestire, miscelandoli tra loro, segnali audio di diversa provenienza, assegnandoli a determinate uscite e modificandoli in una serie di parametri. Si potrebbe anche vederlo come un “controllo remoto unico” di un sistema complesso costituito da un insieme di dispositivi (microfoni, preamplificatori, equalizzatori, amplificatori, ecc.).
Volendo catalogare le differenti funzioni svolte da un mixer, possiamo affermare che esso:

1) pre-amplifica il segnale in ingresso
2) elabora la risposta in frequenza
3) invia il segnale ad altri circuiti di elaborazione esterni
4) somma i diversi ingressi
5) ri-distribuisce i segnali su diverse uscite
6) permette un controllo su tutti i parametri

\subsection{Architettura del mixer}
Nel descrivere l'architettura e le funzioni principali del mixer, nonché le tipologie di utilizzo, bisogna premettere che i mixer possono presentare una varietà considerevole d'impostazione progettuale, ed una gamma di funzioni aggiuntive che possono rendere ardua una classificazione: non è insolito imbattersi in un modello nato per l'amplificazione con caratteristiche che lo rendano anche vocato alla registrazione, e viceversa, oppure in mixer che possano essere usati indifferentemente come mixer “di sala” o mixer “di palco”, come vedremo in seguito.
La configurazione base, illustrata anche in fig. 1, presenta i seguenti componenti, costituiti da elementi modulari con diverse funzioni:

1) moduli d’ingresso
2) barre di missaggio (buss)
3) stadio di uscita (master)

Concettualmente, un mixer non è altro che una serie di circuiti di amplificazione e di modifica del segnale, i quali vengono convogliati verso dei circuiti sommatori (barre di missaggio). Come vedremo in seguito, nelle entrate e uscite analogiche dei mixer digitali vi sono inoltre i convertitori AD/DA, ma l’architettura logica del mixer rimane sostanzialmente invariata, anche se, cambiando la natura fisica del segnale, cambia l’architettura fisica. Noi ci riferiremo per i punti seguenti ai mixer analogici, sempre considerando l’identità concettuale con i mixer digitali, i quali nel loro aspetto operativo non fanno altro che riprodurre, anche se in maniera virtuale, le sezioni e le funzioni dei mixer analogici.

\subsection{Il modulo d’ingresso}

Ogni modulo di ingresso del mixer, schematizzato in fig. 2, è costituito da diverse sezioni:
a) Lo stadio d’ingresso
b) Lo stadio di equalizzazione
c) Le mandate ausiliare
d) Il potenziometro del volume (fader)
e) L’assegnazione di uscita (routing)

\subsection{Lo stadio d’ingresso}

Lo stadio di ingresso del mixer è costituito generalmente da un ingresso microfonico e da un ingresso linea, talvolta coincidenti fisicamente nello stesso connettore, che nella maggioranza dei casi è del tipo usato nei collegamenti microfonici (XLR).
Nel caso in cui l’ingresso linea sia fisicamente diverso da quello microfonico, il connettore più usato è il jack 1/4” (1/4 di pollice), nella versione bilanciata TRS
(Tip, polo caldo, Ring, polo freddo, Sleeve, schermo) o sbilanciata, con Ring e Sleeve coincidenti (vedi fig. 3).

La differenza fondamentale tra i due ingressi consiste nel fatto che l’ingresso microfonico necessita di uno stadio di preamplificazione, per poter portare il segnale audio ad un livello operativo pari a quello dell’ingresso linea.
Come si può vedere anche nella fig. 4, i controlli e le opzioni presenti nello stadio di ingresso sono:
a) L’alimentazione fantasma (“phantom power” o 48V) per alimentare i microfoni a condensatore
b) L’attenuatore (PAD)
c) Il regolatore di guadagno (“trim” o “gain”)
d) L’invertitore di fase (“phase” o Ø) per invertire la fase del segnale.

La combinazione dell’attenuatore e del trimmer di ingresso svolge la fondamentale funzione di stabilire il livello operativo del segnale nel suo percorso all’interno del mixer, e di conseguenza serve a selezionare tutta la gamma possibile di interfacciamento del mixer ai diversi livelli di segnali che possono presentarsi all’ingresso. Esso rappresenta uno dei controlli più importanti di tutta la catena per determinare la qualità del segnale, poiché una regolazione non corretta del segnale d’ingresso può avere come conseguenza:
a) Saturazione (guadagno troppo elevato)
b) Rumore (guadagno insufficiente)

\subsection{Lo stadio di equalizzazione}

Lo stadio di equalizzazione è costituito da una serie di filtri che agiscono sui parametri che determinano la risposta in frequenza. Tali filtri possono essere molto vari, ma la configurazione base è solitamente formata da:
a) LO (low frequency) shelf
b) MID (mid frequency) peak/dip
c) HI (high frequency) shelf
L’equalizzazione shelf, illustrata in fig. 5 dove sono rappresentati simultaneamente gli interventi sia sulle basse che sulle alte frequenze, consiste di una variazione in positivo o in negativo del guadagno limitato alle frequenze al di sotto (nel LO, al di sopra nell’HI) di una certa soglia di intervento stabilita. Nelle versioni più evolute è possibile variare tramite un ulteriore potenziometro tale frequenza di soglia, ed in questo caso si parla di filtro shelf parametrico.

L’equalizzazione passa-banda (peak/dip o band-pass) consiste in un intervento in positivo o in negativo che interessa una banda di frequenze centrata attorno ad una frequenza di riferimento. Tale intervento, evidenziato in fig. 6, è raffigurato con una curva a campana, e la caratteristica della curva è chiamata campanatura del filtro. Nella figura sono evidenziati due punti ( f1 e f2 ) in cui la curva assume il valore di
+/-3 db. La differenza (in Hz) tra questi due punti definisce la larghezza di banda del filtro, secondo la formula:
larghezza di banda = f − f 21
Dividendo la frequenza centrale per la larghezza di banda otteniamo il valore di un parametro, noto come “Q”, che quantifica la campanatura del filtro, secondo la formula:
Q= f f−f
  21

  Dove f è la frequenza centrale (di risonanza) del filtro. Nell’esempio illustrato, supponendo di avere il filtro centrato a 450 Hz e la larghezza di banda compresa tra 180 e 1.200 Hz, avremo:
Q= 450 = 0.44 1200-180
Un filtro si definisce parametrico quando siano presenti i controlli di fequenza, di guadagno e di Q.

Una sezione di equalizzazione di un mixer si definisce parametrica quando sia composta da filtri parametrici, sia shelf che band-pass, mentre si definisce semi-parametrica quando sia composta in parte da filtri parametrici (generalmente band-pass), e in parte da filtri a taglio fisso.
Nella fig. 7 vediamo la schematizzazione di una sezione di equalizzazione parametrica, con le sezioni:
HI shelf
MID HI bandpass MID LO bandpass LO shelf
In una sezione semi-parametrica mancherà il controllo di frequenza sulle sezioni HI e LO (talvolta è presente un pulsante con due opzioni possibili), è può essere assente il controllo di Q sulla (sulle) sezione (sezioni) MID.

\subsection{Insert point}

In questo punto del percorso del segnale, ma talvolta prima della sezione di equalizzazione, troviamo un connettore di ingresso/uscita chiamato “insert”, presente fisicamente sul pannello posteriore insieme ai connettori di ingresso, che ha il compito di inserire nella catena un eventuale effetto esterno, ad es. un compressore.

Abbiamo anche qui il jack 1/4” TRS, ma la connessione è completamente diversa da quanto illustrato nel jack bilanciato: in un solo connettore sono presenti il segnale di mandata (tip-send) ed il segnale di ritorno (ring-return) dall’effetto esterno, e le connessioni sono in questo caso sbilanciate con la massa in comune (vedi fig. 8). Da notare che talvolta la mandata ed il ritorno sono assegnati al contrario sul connettore, per cui prima di utilizzare la presa insert è opportuno fare riferimento al manuale dell’apparecchio. Nei mixer più sofisticati sono presenti sul pannello due distinti connettori TRS per il send ed il return. Generalmente l'inserzione avviene

automaticamente nel momento in cui si infila un connettore nella presa insert, ma talvolta è presente un pulsante sul modulo di ingresso per attivare tale funzione (è generalmente presente nel caso del doppio connettore).

\subsection{Le mandate ausiliare}

Una mandata ausiliaria (“aux send”) consiste in un potenziometro che regola una quantità di segnale destinata ad un circuito sommatore supplementare (“aux buss”) collegato ad una uscita specifica. Il segnale prelevato tramite questo potenziometro può essere di due tipi: “pre-fader”, quando viene prelevato a monte del controllo principale di volume, ed è quindi indipendente da esso, e “post-fader”, quando viene prelevato a valle, ed in questo caso dipende anche dal controllo principale.

Nella fig. 9 è schematizzata una delle possibili disposizioni dei controlli di mandata ausiliaria, con assegnazione pre-post individuale. Alcuni mixer hanno un certo numero di mandate pre-fader ed un certo numero di mandate post-fader, altri hanno, per il medesimo potenziometro, la possibilità di selezionare il tipo di mandata, altri ancora possono operare questa selezione su gruppi di mandate.
La mandata ausiliaria ha generalmente due tipi di utilizzo: come mandata monitor (“foldback”), o come mandata effetti. Nel primo caso, nell'ambito dell'uso del mixer in amplificazione, il controllo servirà ad es. a rimandare il segnale su monitor di palco (nel caso in cui non sia presente un mixer di palco, come vedremo in seguito), o anche su cuffie, e sarà generalmente pre-fader, mentre nel secondo caso servirà ad aggiungere una certa quantità di riverbero, o di altro effetto, al suono presente in quel canale, e sarà generalmente post-fader.

\subsection{Il potenziometro del volume (fader)}

Il potenziometro principale è generalmente del tipo a cursore (slider), ed è di solito fornito di una scala serigrafata, tarata in decibel, dove è indicato il livello operativo nominale, generalmente 10 db sotto la posizione massima di fine corsa. L’importanza di regolare correttamente il guadagno di ingresso ed il pad consiste da una parte nel fatto di poter lavorare il segnale intorno al livello operativo nominale, ottimizzando il rapporto segnale/rumore all’interno del circuito, e dall’altra di poter avere la corretta manualità nella regolazione della corsa del fader.

Normalmente vicino al fader sono presenti due pulsanti (vedi fig. 10): il primo, il “mute”, è un tasto che ha la funzione di chiudere il canale, escludendolo così dal missaggio, mentre il secondo è un tasto di preascolto PFL (“pre-fade listening”), il quale ha la funzione di inviare

il segnale a monte del fader ad una barra detta SOLO, sulla quale arriveranno tutti i canali che avranno tale pulsante attivato, la quale potrà essere ascoltata in cuffia o su un sistema di monitoraggio, in modo da avere un controllo uditivo sul segnale prima del controllo principale. I segnali selezionati in PFL saranno inoltre visibili sugli strumenti di lettura vu-meter o peak-meter (vedi più oltre), anche col fader a zero, in modo di poter regolare il trimmer di ingresso per il livello nominale ottimale. In questo modulo si trova generalmente anche un led indicatore del livello di overload del circuito, utile per trovare il giusto limite di guadagno.
Una variante importante è rappresentata dal fader VCA, dove il potenziometro del volume viene sostituito da un circuito di amplificazione con il controllo in tensione (“Voltage Controlled Amplifier”), ed il fader, in luogo di dosare il segnale in transito, regola il voltaggio di controllo del VCA. Nella fig. 11 sono evidenziate le differenze tra il fader normale e il fader VCA: il segnale audio, che nel fader normale passa fisicamente attraverso il potenziometro, nel fader VCA è elaborato dal circuito controllato in tensione, mentre il potenziometro serve a regolare la tensione di controllo. Uno dei vantaggi maggiori di questo tipo di circuitazione, che non è altro che un servocomando, è la possibilità di assegnare ogni fader a dei gruppi VCA (“VCA groups”), in modo simile all'operazione di routing, permettendo così un controllo centralizzato di gruppi di canali.


Un’altra importante applicazione del fader VCA inerente ai mixer da studio consiste nel poter utilizzare una funzione di automazione del mixer: i livelli di ogni fader possono essere controllati nel tempo agganciando il mixer ad un codice del tempo (“time code”) in sincronia con il nastro (od altro supporto). Il fader VCA consente infine di poter disporre di controlli di MUTE servocomandati e centralizzabili, alla stessa stregua dei gruppi VCA.

\subsection{L’assegnazione di uscita (routing)}

Questa selezione, normalmente effettuata tramite una serie di pulsanti, stabilisce a quali uscite del mixer il nostro segnale debba essere indirizzato. Su ogni mixer è presente una uscita principale stereo (“master”), oltre ad un certo numero di altre uscite (“buss” o “subgroup”) disposte a coppie (1-2, 3-4, ecc., come illustrato in fig. 12).

Sul modulo d’ingresso avremo la possibilità di selezionare l'uscita master o le uscite sub, o entrambe, a seconda dell'uso che vogliamo fare di queste uscite.
Completa la sezione di routing il potenziometro panoramico (PAN- POT), che svolge la funzione di collocare il segnale in un punto dello spazio compreso tra il canale sinistro e quello destro.

Come si può osservare in fig. 13 l’azione del pan-pot combina un graduale fade-out di uno dei due canali ed un graduale fade-in dell’altro. Nel punto in cui entrambi i canali raggiungono uguale livello esso sarà di -3 dB (corrispondente al coefficiente
2 2 = 0.707 )rispetto al livello di entrata, allo scopo di non avere scompensi
sull’energia totale del programma stereo. La funzione del pan-pot si applica sia all’uscita master sia ai gruppi, conformemente alla scelta effettuata su questi pulsanti.
È importante notare come il segnale, che fino ad ora ha viaggiato nel modulo d’ingresso in forma monofonica, dopo il pan-pot continua il suo percorso in forma stereofonica, su una o più coppie di canali.

\subsection{Moduli d’uscita (buss)}

Nel paragrafo dedicato al modulo d'ingresso è stato descritta l'operazione di routing, ovverossia dei controlli a disposizione per assegnare il segnale presente sui moduli d’ingresso alle barre di missaggio (buss), i cui controlli risiedono sui moduli d’uscita. Sui moduli di tali gruppi, oltre al fader che ha la funzione di controllo generale del buss, è solitamente presente un tasto “SUB” che serve a rimandare l'uscita di quel gruppo al master, dopo aver regolato anche la disposizione nello spazio stereofonico attraverso il potenziometro pan-pot.
In tal caso, il gruppo viene utilizzato come controllo generale di un pre-mix di un gruppo di canali, i quali, sui loro moduli d’ingresso, saranno stati impostati per mandare il segnale a quel gruppo e non al master. Nella fig. 14 è evidenziato il fader


del subgroup col tasto SUB ed il potenziometro pan-pot per ri-indirizzare il segnale al modulo master.

Un’ulteriore possibilità di utilizzo delle barre di missaggio è quella di offrire una uscita stereofonica aggiuntiva in parallelo all’uscita master, ad es. per registrare ove il mixer sia impiegato per amplificare. Bisogna aggiungere che i buss possono a loro volta disporre di sezioni di equalizzazione, di insert, e talvolta hanno delle entrate ausiliarie. Spesso dispongono, al pari della sezione master, di strumenti per visualizzare il segnale, tipo i “VU-meter” (Volume Unit) o i “PPM- meter” (Peak Program Meter).
La differenza fondamentale tra questi due tipi di display consiste nel fatto che, mentre il VU-meter visualizza il livello RMS, il PPM-meter visualizza il livello di picco del segnale. Entrambi questi display possono essere del tipo analogico ad ago o del tipo digitale a LED; nel primo caso avremo un’indicazione meno pronta dei transienti a causa dell’inerzia dell’ago (circa 300 msec), ma più precisa sui toni fissi per via del’indicazione continua, mentre nel secondo avremo un’indicazione che risponde con maggiore rapidità (circa 10 msec) alle variazioni del segnale a scapito di una scala discreta dei livelli. Nella
fig. 15 sono evidenziati i display analogici VU-meter e PPM meter, ed anche un indicatore a LED (bargraph meter), che spesso può essere commutato per una lettura VU o PPM.
In questa sezione, ma talvolta anche nel modulo master, si collocano anche i controlli master delle uscite ausiliarie, dotate anche di pulsante SOLO che però, a differenza dei moduli di ingresso, ha la caratteristica di essere in modalità AFL, a valle del potenziometro (“after-fade listening”).

In alcuni mixer più evoluti su questi moduli è presente una sezione “matrix”, che ha la funzione di riassegnare il segnale in uscita dai gruppi indirizzandolo a delle uscite aggiuntive (le uscite matrix, appunto), tramite una serie di potenziometri disposti secondo uno schema a matrice. Nella fig. 16 è illustrato un esempio di matrix a 4 canali su un mixer con 4 buss e uscita master left/right: in orizzontale sono indicati gli ingressi ed in verticale le 4 uscite matrix.

\subsection{Stadio di uscita (master)}
Questa sezione, oltre che ospitare il fader principale dell’uscita left/right, contiene alcuni controlli di carattere generale. E’ una sezione che può differire molto da mixer a mixer, a seconda delle funzioni che il progettista ha voluto includervi. In questo modulo (vedi fig. 17) è sicuramente presente un potenziometro per il livello di ascolto in cuffia (“phones”), mentre il connettore per la stessa, di tipo jack stereo, potrà trovarsi sullo stesso modulo oppure sul pannello posteriore del mixer.

Altro strumento indispensabile è il vu-meter (o peak-meter), che, oltre che nel modulo master, potrà essere alloggiato, per un migliore angolo di lettura, su un’alzata alle spalle dei moduli (“bridge”).
Un altro pulsante che è possibile trovare è quello del talkback, unitamente ad un microfono miniaturizzato o ad un attacco microfonico, ad un controllo di livello, e ad una serie di pulsanti per indirizzare questo segnale. Il talkback è un interfono avente la funzione di mettere in contatto l’operatore al mixer col palco (o con la sala, in studio) o tramite le cuffie, o tramite i monitor, o tramite un altoparlante dedicato.
Esiste poi la possibilità che questo modulo ospiti i controlli per una serie di uscite ausiliarie:
a) Control Room, ossia un controllo di volume per l’ascolto in
regìa
b) Studio, nel caso in cui la sala di registrazione disponga di
diffusori nella sala

c) Rec, ovverossia un potenziometro che regola un’uscita dedicata alla registrazione
Talvolta, il master dispone anche di ingressi ausiliari, come ad es. “effect return”, da miscelare a monte del master fader.
Il modulo master spesso ospita un generatore di toni a una o più frequenze (tipico 100 Hz - 1KHz - 10KHz) assegnabile a varie uscite, con la funzione di taratura e di interfacciamento con apparecchi esterni.
Infine, se il mixer dispone di fader VCA, e quindi di VCA groups e di VCA mute, che analogamente ai VCA groups centralizzano le operazioni di mute su gruppi di canali, sul modulo saranno presenti dei controlli master per queste funzioni.
Nel modulo master dei mixer da live più evoluti trova anche spazio la sezione relativa alle memorie, ossia alla possibilità di memorizzare staticamente (“Snapshot”) i valori di fader e di MUTE, di poter organizzare una sequenza di queste memorie, e di poterle richiamare di volta in volta con un apposito comando di “recall”. In tali mixer è anche possibile, tramite apposite connessioni, “linkare” due o più mixer uguali, per centralizzare la gestione delle memorie e dei gruppi VCA, rendendo possibile il controllo su situazioni live molto complesse con un gran numero di canali audio.
Nei mixer dotati di automazione vi saranno dei controlli centralizzati per impostare questa funzione, che generalmente è limitata ai fader e ai mute e può disporre di questi comandi:
a) Write: il mixer memorizza i movimenti dei fader e dei mute e li sincronizza al Time-Code
b) Read: il mixer esegue l’automazione registrata
c) Update: il mixer corregge solo determinati movimenti di uno o più canali.

\subsection{Schemi circuitali}

Per concludere questa panoramica sull’architettura del mixer, è importante dare uno sguardo a come convenzionalmente vengono schematizzati i collegamenti e i vari stadi di trattamento del segnale, secondo una grafica universalmente accettata. La fig. 18 esemplifica un mixer nelle sue componenti fondamentali.

\subsection{I mixer da live}
Il mixer fin qui descritto può agevolmente essere classificato tra quelli utilizzati nell’amplificazione, ed in particolare nei mixer “di sala” (FOH - front of house), i mixer cioè adibiti al controllo dei suoni per la zona riservata al pubblico. Nell’ambito dell’amplificazione può essere presente, ed è indispensabile in situazioni complesse, il mixer “di palco”.
Tale mixer, schematizzato in fig. 19, ha il compito di controllare il suono inviato ai diffusori (“monitor”) utilizzati dai musicisti sul palco per poter ascoltare il proprio strumento e per poter ascoltarsi a vicenda senza essere infastiditi dal ritorno sul palco dell’amplificazione di sala. Per espletare questa funzione, il mixer deve essere in grado di fornire un certo numero di missaggi diversi ed indipendenti ai vari monitor, e la conseguenza che ne deriva è che il master fader stereo diventa solo un controllo d’ascolto del fonico di palco, mentre i controlli master reali diventano le uscite master delle mandate ausiliarie, ad ognuna delle quali sarà collegato un monitor di palco. A tali master il segnale sarà indirizzato dai moduli d'ingresso tramite potenziometri, esattamente allo stesso modo delle mandate ausiliarie già descritte in precedenza. Il fader associato ad ogni canale avrà la funzione di controllo master del canale stesso.

Il mixer di palco, naturalmente, è adoperato congiuntamente al mixer di sala, e ne condivide le sorgenti sonore, cioè i microfoni e quant’altro, i quali saranno quindi collegati ai due mixer o con un semplice parallelo dei connettori (“splitter passivo”), o tramite un circuito elettronico (“splitter attivo”). Nel primo caso è importante che l'eventuale alimentazione phantom sia fornita da uno solo dei mixer, mentre nel secondo saranno gli stessi splitter a svolgere questo compito.

\subsection{I mixer da studio}
I mixer adibiti alla registrazione, detti anche “da studio”, presentano in aggiunta una sezione “tape return”, consistente in connettori d’ingresso, fader, pan-pot, e talvolta ulteriori sezioni di equalizzazione, con il compito di ricevere e miscelare l’output, ovverossia il “ritorno macchina” di un registratore multitraccia. Esistono due architetture per tali sezioni: “split” e “in-line”.

Nella configurazione split (fig. 20) la sezione di tape return è ospitata nei moduli master dei gruppi di uscita, generalmente a destra del modulo master, mentre nel mixer in-line è ospitata nei moduli d'ingresso (talvolta è identificata con il nome MIX B, vedi fig. 21). Questa configurazione presenta diversi vantaggi, tra i quali la possibilità di ospitare un numero di ritorni pari al numero dei canali del mixer, e la possibilità, attraverso un pulsante (“flip”), di assegnare il controllo di volume e la sezione di equalizzazione ora al modulo d'ingresso, ora al ritorno macchina.


\subsection{Gli outboards}

Gli outboards sono costituiti da tutta quella serie di apparecchi che completa l’elaborazione del segnale audio aggiungendo effetti che o sono presenti nei mixer in forma semplificata o non sono presenti affatto. Sono quindi apparecchi esterni (“stand-alone”), oppure possono svolgere le loro funzioni in forma virtuale all’interno delle workstation digitali, sotto forma di “plug- in”. Non è infrequente incontrare dei plug-in che riproducono in tutto e per tutto l’estetica dei pannelli di controllo del corrispondente hardware, oltre ad essere in grado di svolgerne tutte le funzioni. Quello che ci interessa approfondire sono le caratteristiche funzionali degli outboards, e quindi le considerazioni che esporremo rimangono valide sia per i dispositivi hardware che software.
Il campo degli outboards è piuttosto vasto, ci occuperemo qui delle principali elaborazioni, ed in particolare:
1) Equalizzatori
2) Compressori
3) Riverberi
4) Delay

\subsection{Gli equalizzatori}

Dell’equalizzazione abbiamo parlato nella lezione dedicata al mixer, ma mentre nei canali di input del mixer sono presenti equalizzatori per lo più parametrici o semi-parametrici, tra gli outboard è possibile trovare una serie più ampia di tipologie. Qui parleremo più diffusamente degli equalizzatori grafici.
Equalizzatori grafici
Alla base dei circuiti equalizzatori noi troviamo un filtro, che nella sua forma più elementare è noto come “Filtro RC” (fig. 1).


Quello rappresentato in figura (un filtro passa-alto, come vedremo tra poco) è una combinazione di una resistenza e di un condensatore (RC), ed appartiene alla categoria dei filtri passivi, ossia è costituito da un circuito in cui non è
presente alcun semiconduttore con funzioni di amplificazione (transistor, circuito operazionale, ecc.).
Semplici varianti di questa circuitazione ci portano ad avere le seguenti tipologie (fig. 2):
a. filtri passa-alto (“high-pass filter” o HPF)
b. filtri passa-basso (“low-pass filter” o LPF)
c. filtri passa-banda (“band-pass filter” o BPF)
d. filtri a reiezione di banda (“band-rejection filter” o “notch filter”)

I filtri passa-alto (passa-basso), come abbiamo accennato nella lezione sul mixer, hanno il compito di escludere le frequenze al di sotto (al di sopra) di un punto di taglio, e di far passare tutte le frequenze al di sopra (al di sotto), con la maggiore linearità possibile. Tale punto di taglio, espresso in Hertz, è convenzionalmente il valore frequenziale con guadagno pari a -3dB.
In ognuna di queste configurazioni la regione di frequenze nella quale esse sono attenuate si chiamerà “stop-band”, mentre la regione nella quale non sono attenuate si chiamerà “pass-band”.
Se osserviamo la fig. 3 possiamo notare come nella realtà i filtri si comportino in un modo lontano da quello ideale. La transizione dalla zona di pass-band alla zona di stop-band comporta delle oscillazioni (ripple), che formano un parametro di valutazione del filtro.

Un altro parametro che descrive il filtro è costituito dalla “pendenza” (slope), che descrive la rapidità di attenuazione del segnale oltre il punto di taglio (vedi fig. 4), ed è quantificato secondo la seguente tabella:

La pendenza indicata in dB/ottava si riferisce all’attenuazione prodotta dal filtro al raddoppio della frequenza, mentre la notazione in dB/decade descrive la stessa attenuazione riferita al decuplicarsi della frequenza. Tale

coefficiente esprime l’ordine del filtro, per cui un filtro a 6 dB/ottava è un filtro del primo ordine, un filtro a 12 dB/ottava del secondo ordine, ecc. Il filtro visualizzato in fig. 1 è un filtro del primo ordine, mentre se il circuito viene ripetuto una volta in serie diventa del secondo ordine, e così via.
Ogni filtro introduce delle elaborazioni, oltre che sulla risposta in frequenza, anche nella risposta in fase e nel tempo (risposta al rumore impulsivo), per cui esistono diverse tipologie circuitali di filtri che ottimizzano ora l’uno ora l’altro di questi parametri, di cui vediamo in fig. 5 evidenziati i più significativi.

In maniera molto sintetica queste sono le loro caratteristiche:
• Butterworth: migliore risposta in frequenza a scapito della risposta in
fase
• Bessel: migliore risposta in fase a scapito della risposta in frequenza
• Chebyshev: compromesso tra i due a scapito del ripple

Un equalizzatore grafico è composto da un certo numero di bande, ognuna delle quali dispone di un unico controllo che può avere la funzione di esaltazione (passa-banda), o di attenuazione (reiezione di banda). La rappresentazione grafica dell’intervento di questi filtri è evidenziata in fig. 6.
