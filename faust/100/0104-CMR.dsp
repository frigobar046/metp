// COMMON MODE REJECTION
import("stdfaust.lib");
// unbalanced signal source
uSig = os.osc(1000);
// balanced push-pull signals
bal = _ <: _, 0-_;
// external noise injection
extNoise = par(i, 2, _ + no.noise);
// unbalanced receiver
uBal = _+(0-_) : /(2);
process = uSig : bal : extNoise : uBal;