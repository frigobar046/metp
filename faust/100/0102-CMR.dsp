import("stdfaust.lib");
// unbalanced signal source
uSig = os.osc(1000);
// balanced push-pull signals
bal = _ <: _, 0-_;
process = uSig : bal;