import("stdfaust.lib");
// unbalanced signal source
uSig = os.osc(1000);
// balanced push-pull signals
bal = _ <: _, 0-_;
// external noise injection
extNoise = par(i, 2, _ + no.noise);
process = uSig : bal : extNoise;